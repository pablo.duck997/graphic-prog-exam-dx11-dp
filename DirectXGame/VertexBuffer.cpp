#include "VertexBuffer.h"
#include "GraphicsEngine.h"

VertexBuffer::VertexBuffer():m_layout(0), m_buffer(0)
{
}

VertexBuffer::~VertexBuffer()
{
}

bool VertexBuffer::Load(void* listVertices, UINT sizeVertex, UINT sizeList, void* shaderByteCode, UINT sizeByteShader)
{
	if (m_buffer)m_buffer->Release();
	if (m_layout)m_layout->Release();

	D3D11_BUFFER_DESC buff_desc = {};
	buff_desc.Usage = D3D11_USAGE_DEFAULT;
	buff_desc.ByteWidth = sizeVertex * sizeList;
	buff_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buff_desc.CPUAccessFlags = 0;
	buff_desc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA init_data = {};
	init_data.pSysMem = listVertices;

	m_sizeVertex = sizeVertex;
	m_sizeList = sizeList;

	if (FAILED(GraphicsEngine::Get()->m_D3DDevice->CreateBuffer(&buff_desc, &init_data, &m_buffer)))
	{
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		//SEMANTIC NAME - SEMANTIC INDEX - FORMAT - INPUT SLOT - ALIGNED BYTE OFFSET - INPUT SLOT CLASS - INSTANCE DATA STEP RATE
		{"POSITION", 0,  DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,D3D11_INPUT_PER_VERTEX_DATA ,0},
		{ "COLOR", 0,  DXGI_FORMAT_R32G32B32_FLOAT, 0, 12,D3D11_INPUT_PER_VERTEX_DATA ,0 },
		{ "COLOR", 1,  DXGI_FORMAT_R32G32B32_FLOAT, 0, 24,D3D11_INPUT_PER_VERTEX_DATA ,0 }
	};

	UINT size_layout = ARRAYSIZE(layout);

	if (FAILED(GraphicsEngine::Get()->m_D3DDevice->CreateInputLayout(layout, size_layout, shaderByteCode, sizeByteShader, &m_layout)))
	{
		return false;
	}

	return true;

}

UINT VertexBuffer::GetSizeVertexList()
{
	return this -> m_sizeList;
}

bool VertexBuffer::Release()
{
	m_layout->Release();
	m_buffer->Release();
	delete this;
	return true;
}

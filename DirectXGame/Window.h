#pragma once
#include <Windows.h>

class Window
{
public:
	Window();
	~Window();

	bool Init();
	bool Broadcast();
	bool IsRun();
	bool Release();

	RECT GetClientWindowRect();
	void SetHWND(HWND hwnd);

	//Events
	virtual void OnCreate() = 0;
	virtual void OnUpdate() = 0;
	virtual void OnDestroy();

protected:
	HWND m_hwnd;
	bool m_isRun;

};


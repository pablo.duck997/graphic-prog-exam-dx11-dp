
#pragma once
#include "InputListener.h"
#include <unordered_set>

class InputSystem
{
public:
	InputSystem();
	~InputSystem();

	void Update();
	void AddListener(InputListener* listener);
	void RemoveListener(InputListener* listener);

public:
	static InputSystem* Get();
private:
	std::unordered_set<InputListener*> m_set_listeners;
	unsigned char m_keys_state[256] = {};
	unsigned char m_old_keys_state[256] = {};
};
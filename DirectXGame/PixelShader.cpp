#include "PixelShader.h"
#include "GraphicsEngine.h"

PixelShader::PixelShader()
{
}

PixelShader::~PixelShader()
{
}

bool PixelShader::Release()
{
	m_ps->Release();
	delete this;
	return true;
}

bool PixelShader::Init(const void* shader_byte_code, size_t byte_code_size)
{
	if (!SUCCEEDED(GraphicsEngine::Get()->m_D3DDevice->CreatePixelShader(shader_byte_code,
		byte_code_size, nullptr, &m_ps))) return false;

	return true;
}
